# apiTask



## Getting started

Test task for fabrique.studio job vacancy

## Installation
`git clone https://gitlab.com/m15h4nya/apitask.git`

## Starting
```
cd apitask
docker build -t apitask:latest .
docker run -dp 8080:8080 --name apitask apitask:latest
```

Now the service is listening on port 8080

## Additional tasks

implemented additional task #5
